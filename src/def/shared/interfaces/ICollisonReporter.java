package def.shared.interfaces;

public interface ICollisonReporter {
	public void collision( final IPhysicsObjectData a, final IPhysicsObjectData b );
}
