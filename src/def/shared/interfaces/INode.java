package def.shared.interfaces;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import def.shared.math.Radian;
import def.shared.util.ITree;

public interface INode {

	public void setParent(final INode parent);

	public INode getParent();

	public void addChild(final INode child);

	public void notifyChildrenOfUpdate();

	public void updatePos(final Vector3f pos);

	public void setPos(final Vector3f pos);

	public Vector3f getPosition();

	public void updateRotation(Matrix3f mtx);

	public void updateRotation(Vector3f axis, Radian angle);
	
	public void setRotatation(Matrix3f mtx);

	public void setFullMatrixTransformationOverride(final Matrix4f mtx);
	
	public void setFullMatrixTransformationOverride(final float mtx[]);

	public float getScale();

	public void setScale(float scale);

	public Matrix4f getAllTransformsCopy();

	public Matrix4f getAllTransforms();

	public String getName();

	public void setInheritOrientation(boolean inheritOrientation);

	public boolean isInheritOrientation();

	public void setInheritScale(boolean inheritScale);

	public boolean isInheritScale();
	
	public void setParentIsUpdated();
	
	public ITree<INode> getTree();

}