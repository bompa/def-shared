package def.shared.interfaces;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import def.shared.math.Radian;

public interface IPositionable 
{
	
	/**
	 * Translate relative
	 * @param transl
	 */
	public void move(final Vector3f transl);
	/**
	 * Set position
	 * @param transl
	 */
	public void setPosition(float[] pos);
	public void setPosition(final Vector3f pos);
	public Vector3f getPosition();
	
	public void setScale(float scale);
	public float getScale();
	
	public void setFullMatrixTransformations(final float mtx[]);
	public void setFullMatrixTransformations(final Matrix4f mtx);
	
	/**
	 * Update rotation relatively
	 * @param axis
	 * @param angle
	 */
	public void rotate(Vector3f axis, Radian angle);
	public void rotate(Matrix3f mtx);
	
	public void setRotation(float[] mtx);
	public void setRotation(Matrix3f mtx);
	
	Matrix4f getModelMatrixCopy();
	Matrix4f getModelMatrix();
	
	public INode getNodeReadOnly();
	public INode getNode();
	public void setSceneParent(IPositionable parent);
	public void addSceneChild(final IPositionable child);
}
