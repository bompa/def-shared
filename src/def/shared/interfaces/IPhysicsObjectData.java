package def.shared.interfaces;


public interface IPhysicsObjectData {
	public IPositionable getIPostionable();
	
	/**
	 * Get object ref id which identifies the physics object
	 * @return
	 */
	public Object getPhysicsObjectRefID();

	/**
	 * Set object ref ID.
	 * @return
	 */
	public void setPhysicsObjectRefID(final Object obj);
	
	public Object getOptional();
	
	public void setOptional(final Object opt);
	
}
