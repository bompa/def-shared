package def.shared.interfaces;

public interface IRayHitReporter {
	public void rayHit(float hitDirx, float hitDiry, float hitDirz, final IPhysicsObjectData hitObject);
}
