package def.shared.util;

public class OutArg<T> {
	public T value;
	
	public OutArg(T value) {
		this.value = value;
	}
}
