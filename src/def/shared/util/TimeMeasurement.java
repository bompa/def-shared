package def.shared.util;

import java.util.concurrent.atomic.AtomicLong;

public class TimeMeasurement {
	private long timeStamp = 0;
	private long frequency = 0;
	private long periodTimeMs = 0;
	private AtomicLong updateCount = new AtomicLong(0);
	
	public static TimeMeasurement create(){
		return new TimeMeasurement();
	}
	
	private TimeMeasurement(){
		reset();
	}
	
	public void reset(){
		timeStamp = 0;
		frequency = 0;
		periodTimeMs = 0;
		updateCount.set(0);
	}
	
	public void timeStamp(){
		updateCount.incrementAndGet();
		
		long currentTimeMs = System.currentTimeMillis();
		periodTimeMs = currentTimeMs - timeStamp;
		timeStamp = currentTimeMs;
		
		frequency = ((long)((1.0 / (double)periodTimeMs) * 1000));
	}

	public long getFrequency() {
		return frequency;
	}
	public long getPeriodTimeMs(){
		return periodTimeMs;
	}
	public long getNrOfTimeStampUpdates(){
		return updateCount.get();
	}
	public void resetNrOfTimeStampUpdates(){
		updateCount.set(0);
	}
}
