package def.shared.util;

public interface ECloneable<E> extends Cloneable {
	public E clone();
}
