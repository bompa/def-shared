package def.shared.util;

import java.util.concurrent.LinkedBlockingQueue;

public class ConcurrentObservable<E> implements IObservable<E>{
	private final LinkedBlockingQueue<IObserver<E>> queue = new  LinkedBlockingQueue<IObserver<E>>();

	public void notifyObservers(E data) {
		for(IObserver<E> obs : queue){
			obs.update(data);
		}
	}

	public void registerObserver(IObserver<E> observer) {
		queue.add(observer);
	}

	public void removeObserver(IObserver<E> observer) {
		
	}
}
