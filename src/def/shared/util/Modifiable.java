package def.shared.util;

public interface Modifiable<T extends Modifiable<T>> extends Cloneable {
    T clone();
}
