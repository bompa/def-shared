package def.shared.util;

public interface IObserver<E> {
	public void update(E data);
}
