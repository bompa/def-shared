package def.shared.util;

import java.util.Collection;

public interface ITree<T> {

	public void addLeaf(T root, T leaf);

	public ITree<T> addLeaf(T leaf);
	
	public ITree<T> addLeaf(final ITree<T> tree);

	public ITree<T> setAsParent(T parentRoot);

	public T getTreeNode();

	public ITree<T> getTree(T element);

	public ITree<T> getParent();

	public Collection<T> getSuccessors(T root);

	public Collection<? extends ITree<T>> getSubTrees();
	
	public void traverseDepthFirst(NodeVisitor<T> visitor);
	
	public static interface NodeVisitor<T>{
		public void visit(T node);
	}
}