package def.shared.util;

public interface IObservable<E> {
	public void registerObserver(IObserver<E> observer);
	public void removeObserver(IObserver<E> observer);
	public void notifyObservers(E data);
}
