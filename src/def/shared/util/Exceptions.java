package def.shared.util;

public class Exceptions {
	public static String stackTraceToString(Throwable t){
		StringBuilder strBuilder = new StringBuilder();
		for(StackTraceElement st : t.getStackTrace()){
			strBuilder.append(st.toString()).append('\n');
		}
		return strBuilder.toString();
	}
}
