package def.shared.util;

public class RestrictInterfaceWrapper<E>{
	
	private final E wrappedContent_;
	private final String unsupportedMsg_;
	
	public RestrictInterfaceWrapper(final E wc, final String msg){
		wrappedContent_= wc;
		unsupportedMsg_ = msg;
	}

	protected E getWrappedContent() {
		return wrappedContent_;
	}
	
	protected void handleUnsupported(){
		throw new UnsupportedOperationException(unsupportedMsg_);
	}
}
