package def.shared.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


public class Tree<T> implements ITree<T>{
	private final T treeNode;
	private final ArrayList<Tree<T>> leafs = new ArrayList<Tree<T>>();
	private ITree<T> parent = null;
	private final ITreeNodeFactory<T> nodeFactory;
	private HashMap<T, Tree<T>> locate = new HashMap<T, Tree<T>>();
	
	public Tree(final T treeNode) {
		nodeFactory = DefaultTreeNodeFactory.create();
		
		this.treeNode = treeNode;
		locate.put(treeNode, this);
		
	}

	public Tree(final T head, final ITreeNodeFactory<T> nodeFactory) {
		if(nodeFactory == null)
			throw new IllegalArgumentException("ATree created with nodeFactory=null");
		
		this.treeNode = head;
		this.nodeFactory = nodeFactory;
		locate.put(head, this);
	}

	/**
	 * Add leaf
	 */
	public void addLeaf(T root, T leaf) {
		if (locate.containsKey(root)) {
			locate.get(root).addLeaf(leaf);
		} else {
			addLeaf(root).addLeaf(leaf);
		}
	}

	/**
	 * Create new tree and add as leaf
	 */
	public ITree<T> addLeaf(T leaf) {
		Tree<T> t = nodeFactory.createNode(leaf);
		addLeaf(t);
		return t;
	}
	
	/**
	 * Add existing tree as leaf
	 * @param t
	 * @return
	 */
	public ITree<T> addLeaf(final ITree<T> tIn) {
		final Tree<T> t = (Tree<T>) tIn; 
		leafs.add(t);
		t.parent = this;
		t.locate = this.locate;
		locate.put(t.treeNode, t);
		return t;
	}


	public ITree<T> setAsParent(T parentRoot) {
		Tree<T> t = nodeFactory.createNode(parentRoot);
		t.leafs.add(this);
		this.parent = t;
		t.locate = this.locate;
		t.locate.put(treeNode, this);
		t.locate.put(parentRoot, t);
		return t;
	}

	public T getTreeNode() {
		return treeNode;
	}

	public ITree<T> getTree(T element) {
		return locate.get(element);
	}

	public ITree<T> getParent() {
		return parent;
	}

	public Collection<T> getSuccessors(T root) {
		Collection<T> successors = new ArrayList<T>();
		Tree<T> tree = (Tree<T>) getTree(root);
		if (null != tree) {
			for (Tree<T> leaf : tree.leafs) {
				successors.add(leaf.treeNode);
			}
		}
		return successors;
	}

	public Collection<? extends ITree<T>> getSubTrees() {
		return leafs;
	}

	public static <T> Collection<T> getSuccessors(T of, Collection<Tree<T>> in) {
		for (Tree<T> tree : in) {
			if (tree.locate.containsKey(of)) {
				return tree.getSuccessors(of);
			}
		}
		return new ArrayList<T>();
	}
	
	public void traverseDepthFirst(NodeVisitor<T> visitor){
		visitor.visit(getTreeNode());
		
		for (Tree<T> child : leafs) {
			child.traverseDepthFirst(visitor);
		}
	}

	@Override
	public String toString() {
		return printTree(0);
	}

	private static final int indent = 2;

	private String printTree(int increment) {
		String s = "";
		String inc = "";
		for (int i = 0; i < increment; ++i) {
			inc = inc + " ";
		}
		s = inc + treeNode;
		for (Tree<T> child : leafs) {
			s += "\n" + child.printTree(increment + indent);
		}
		return s;
	}
	
	public static interface ITreeNodeFactory<E>{
		public Tree<E> createNode(E node);
	}
	
	public static class DefaultTreeNodeFactory<E> implements ITreeNodeFactory<E>{
		public static <T> DefaultTreeNodeFactory<T> create(){
			return new DefaultTreeNodeFactory<T>();
		}
		
		private DefaultTreeNodeFactory(){}
		
		public Tree<E> createNode(E node) {
			return new Tree<E>(node, this);
		}
	}
}
