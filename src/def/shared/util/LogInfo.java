package def.shared.util;

public class LogInfo {
	public LogInfo(String msg, int level){
		msg_ = msg;
		level_ = level;
	}
	public String msg_;
	public int level_;
	
	public void clear(){
		msg_ = null;
		level_ = -1;
	}
	
	public boolean valid(){
		return msg_ != null && level_ > 0;
	}
}
