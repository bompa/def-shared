package def.shared.math;

public class Degree {
	public Degree(float value){
		this.value = value;
	}
	
	public static Degree valueOf(Radian r){
		return null;
	}
	
	public String toString(){
		return value + "degrees";
	}
	
	public float value;
	
	/**
	 * Converts radian value to degrees
	 * @param r radians
	 * @return degres
	 */
	public static float degreeValue(float r)
	{
		return (float) (r * (180.0f / Math.PI));
	}
}
