package def.shared.math;

public class Radian {
	public Radian(float value){
		this.value = value;
	}
	public float value;

	public static float degreeValue(float r){
		return degreeValue(new Radian(r));
	}
	public static float degreeValue(Radian r){
		return (float) (r.value / 360.0f * Math.PI);
	}
	
	public static Radian quarter(){
		return new Radian((float)Math.PI / 2.0f);
	}
	
	public static Radian half(){
		return new Radian((float)Math.PI);
	}
	
	public static Radian full(){
		return new Radian((float)Math.PI * 2.0f);
	}
}
